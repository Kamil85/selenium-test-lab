import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_16_Test_Tworzenia_Procesu_Test extends SeleniumBaseTest {

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    String processName = UUID.randomUUID().toString().substring(0, 10);


    @Test
    public void addProcessTest() {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .submitCreate()
                .assertProcess(processName,"","");
        }
    }




