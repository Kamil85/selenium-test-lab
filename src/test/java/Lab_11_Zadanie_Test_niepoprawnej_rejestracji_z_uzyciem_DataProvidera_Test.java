import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

public class Lab_11_Zadanie_Test_niepoprawnej_rejestracji_z_uzyciem_DataProvidera_Test extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegisterEmail(String wrongEmail) {

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterLnk();
        createAccountPage.typeEmail(wrongEmail);
        createAccountPage.registerWithFailure();

        Assert.assertEquals(createAccountPage.emailError.getText(), "The Email field is not a valid e-mail address.");
        Assert.assertTrue(createAccountPage.emailError.isDisplayed());

    }
}