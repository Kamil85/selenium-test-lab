import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_9_Test_Niepoprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTestwithChaining() {

        new LoginPage(driver)
                .typeEmail("test@testt.com")
                .typePassword(config.getApplicationPassword())
                .submitLoginWithFailure()
                .assertLoginError("Invalid login attempt.");



    }
}