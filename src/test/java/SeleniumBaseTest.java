import config.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest {

    protected WebDriver driver;
    protected Config config;

    @BeforeMethod
    public void baseBeforeMethod() {

        config = new Config();
        System.setProperty("webdriver.chrome.driver", config.getChromedriverPath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(config.getApplicationURL());


    }

    @AfterMethod
    public void baseAfterMethod(){
        driver.quit();
    }
}
