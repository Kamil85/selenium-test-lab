import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void correctLoginTestwithChaining() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertWelcomeElementIsShown();
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.typeEmail("test@test.com");
//        loginPage.typePassword("Test1!");
//
//        HomePage homePage = loginPage.submitLogin();
//
//        homePage.assertWelcomeElementIsShown();

    }
}
