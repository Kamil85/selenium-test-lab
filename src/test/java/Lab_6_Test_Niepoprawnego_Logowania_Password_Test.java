import config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_6_Test_Niepoprawnego_Logowania_Password_Test {

    Config config;

           @Test
        public void correctLoginTest() {
            System.setProperty("webdriver.chrom.driver", "C:/dev/driver/chromdriver.com");
            WebDriver driver = new ChromeDriver();

            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(new Config().getApplicationURL());

            WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
            emailTxt.sendKeys("test@test.com");

            WebElement paswordTxt = driver.findElement(By.cssSelector("input[type=password]"));
            paswordTxt.sendKeys("Test1");

            WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
            loginBtn.click();

            WebElement passwordError = driver.findElement(By.cssSelector(".text-danger.validation-summary-errors li"));
            Assert.assertEquals(passwordError.getText(), "Invalid login attempt.");

            List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
            boolean doesErrorExists = validationErrors
                    .stream()
                    .anyMatch(validationError -> validationError.getText().equals("Invalid login attempt."));
            Assert.assertTrue(doesErrorExists);

            driver.quit();

        }
    }

