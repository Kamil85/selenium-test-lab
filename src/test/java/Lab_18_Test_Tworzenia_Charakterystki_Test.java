import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_18_Test_Tworzenia_Charakterystki_Test extends SeleniumBaseTest {
    @Test
    public void addCharacteristics(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .loginData(config.getApplicationUser(),config.getApplicationPassword())
                .goToCharacteristics()
                .addNewCharacteristics()
                .typeName(processName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate();

    }

}
