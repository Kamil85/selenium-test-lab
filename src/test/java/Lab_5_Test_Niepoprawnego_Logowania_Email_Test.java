import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test {
    @Test
    public void correctLoginTest() {
        System.setProperty("webdriver.chrom.driver", "C:/dev/driver/chromdriver.com");
        WebDriver driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");

        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("testtest");

        WebElement paswordTxt = driver.findElement(By.cssSelector("input[type=password]"));
        paswordTxt.sendKeys("Test1!");

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();

        WebElement emailError = driver.findElement(By.id("Email-error"));
        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
        boolean doesErrorExists = validationErrors
                .stream()
                .anyMatch(validationError -> validationError.getText().equals("The Email field is not a valid e-mail address."));
        Assert.assertTrue(doesErrorExists);

        driver.quit();

    }
}