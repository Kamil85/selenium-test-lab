import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_17_Negatywny_Proces_Test extends SeleniumBaseTest {


    @Test
    public void addProcessWithFailure() {
        String shortProcessName = "ab";
        String expErrorMessege = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";
        new LoginPage(driver)
                .loginData(config.getApplicationUser(), config.getApplicationPassword())
                .goToProcesses()
                .clickAddProcess()
                .typeName(shortProcessName)
                .submitCreateWithFailure()
                    .assertProcessNameError(expErrorMessege)
                .backtToList()
                    .assertProcessIsNotShown(shortProcessName);

    }
}
