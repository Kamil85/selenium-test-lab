import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Menu_Test extends SeleniumBaseTest {
    @Test
    public void menuTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessesCorrectURL()
                .assertProcessesHeaderIsShown()
                .goToCharacteristics()
                .assertCorrectCharacteristicURL()
                .assertCharacteristicsHeaderIsShown()
                .goToDashboard()
                .assertDashboardHeaderIsShown();


    }
}
