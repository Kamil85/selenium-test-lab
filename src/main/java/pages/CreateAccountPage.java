package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {

    protected WebDriver driver;


    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;
    @FindBy(css = "#Password")
    private WebElement passwordTxt;
    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;
    @FindBy(css = "#Email-error")
    public WebElement emailError;
    @FindBy(css = ".validation-summary-errors li")
    public List<WebElement> loginErrors;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage registerWithFailure() {
        registerBtn.click();
        return this;
    }

    public CreateAccountPage assertLoginError(String expectedError) {
        Assert.assertEquals(emailError.getText(), expectedError);
        Assert.assertTrue(emailError.isDisplayed());
        return this;
    }

    public CreateAccountPage assertEmailError(String expectedError) {
        Assert.assertEquals(emailError.getText(), expectedError);
        return this;
    }
}

