package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }


    public CharacteristicsPage assertCorrectCharacteristicURL() {
        driver.getCurrentUrl().contains("http://localhost:4444/Characteristics");
        return this;
    }

    @FindBy(css = ".x_title>h2")
    private WebElement processesTxt;

    @FindBy(css = "a[href*=Create]")
    WebElement addCharacteristicsBtn;


    public CharacteristicsPage assertCharacteristicsHeaderIsShown() {
        Assert.assertTrue(processesTxt.isDisplayed());
        Assert.assertEquals(processesTxt.getText(), "Characteristics");
        return this;
    }

    public CreateCharacteristicPage addNewCharacteristics(){
        addCharacteristicsBtn.click();
        return new CreateCharacteristicPage(driver);
    }
}
