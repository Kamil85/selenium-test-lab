package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {
    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = ".menu-home")
    WebElement homeMenu;

    @FindBy(css = ".menu-workspace")
    WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects]")
    WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    WebElement characteristicsMenu;

    @FindBy(xpath = "//*[@id=\"sidebar-menu\"]/div/ul/li[1]/ul/li/a" )
    WebElement dashboardMenu;

    private boolean isPaternExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));        // ".//."- xpath do rodzica
        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses() {
        if (!isPaternExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));
        processesMenu.click();
        return new ProcessesPage(driver);
    }

    public CharacteristicsPage goToCharacteristics() {
        if (!isPaternExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicsMenu));
        characteristicsMenu.click();
        return new CharacteristicsPage(driver);

    }

    public DashboardPage goToDashboard(){
        if(!isPaternExpanded(dashboardMenu)){
            homeMenu.click();
        }
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));
        dashboardMenu.click();
        return new DashboardPage(driver);
    }

    public HomePage assertWelcomeElementIsShown() {
        Assert.assertTrue(welcomeElm.isDisplayed(), "Welcome element is not shown.");
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"), "Welcome element text: '" + welcomeElm.getText() + "' does not contain word 'Welcome'");

        return this;

    }
}