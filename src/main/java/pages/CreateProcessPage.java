package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessPage extends HomePage {

    public CreateProcessPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "input[type=submit]")
    WebElement createBtn;

    @FindBy(css = "#Name")
    WebElement processNameDesc;

    @FindBy(css = ".field-validation-error")
    WebElement createProcessError;

    @FindBy(css = " div:nth-child(6)  a[href*=Project]")
    WebElement backtToListBtn;


    public CreateProcessPage typeName(String a) {
        processNameDesc.clear();
        processNameDesc.sendKeys(a);
        return this;
    }

    public ProcessesPage submitCreate() {
        createBtn.click();
        return new ProcessesPage(driver);
    }

    public CreateProcessPage submitCreateWithFailure(){
        createBtn.click();
        return this;
    }

    public ProcessesPage backtToList(){
        backtToListBtn.click();
        return new ProcessesPage(driver);
    }



    public CreateProcessPage assertProcessNameError(String expErrorMessege) {
        Assert.assertTrue(createProcessError.isDisplayed());
        Assert.assertEquals(createProcessError.getText(), expErrorMessege);
        return this;
    }

}




