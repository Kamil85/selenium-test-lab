package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {

    protected WebDriver driver;
    private String mail;
    private String password;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;
    @FindBy(css = "#Password")
    private WebElement passwordTxt;
    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;
    @FindBy(css = ".validation-summary-errors li")
    public List<WebElement> loginErrors;
    @FindBy(css = "#Email-error")
    public WebElement emailError;
    @FindBy(css = "a[href*=Register]")
    public WebElement registerLnk;

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;

    }

    public CreateAccountPage goToRegisterLnk() {
        registerLnk.click();
        return new CreateAccountPage(driver);
    }

    public HomePage loginData(String mail, String password) {
        typeEmail(mail);
        typePassword(password);
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage assertLoginError (String expectedError){
        Assert.assertEquals(loginErrors.get(0).getText(),expectedError);
        Assert.assertTrue(loginErrors.get(0).isDisplayed(),"No errors display");
        return this;
    }

    public LoginPage assertEmailError (String expectedError){
        Assert.assertEquals(emailError.getText(), expectedError);
        return this;
    }
}

