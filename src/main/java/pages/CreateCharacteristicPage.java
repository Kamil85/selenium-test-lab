package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CreateCharacteristicPage extends HomePage {
    protected WebDriver driver;

    public CreateCharacteristicPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;
    @FindBy(id = "Name")
    private WebElement nameTxt;
    @FindBy(xpath = "//*[@id=\"Name\"]")
    private WebElement lowerSpecificationLimit;
    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecificationLimit;
    @FindBy(id = "HistogramBinCount")
    private WebElement histogramBinCount;
    @FindBy(css = "input[type=submit]")
    private WebElement cretateCharacteristicsBtn;


    public CreateCharacteristicPage typeName(String a) {
        nameTxt.click();
        nameTxt.clear();
        nameTxt.sendKeys(a);
        return this;
    }

    public CreateCharacteristicPage typeLsl(String a) {
        lowerSpecificationLimit.click();
        lowerSpecificationLimit.sendKeys(a);
        return this;
    }

    public CreateCharacteristicPage typeUsl(String a) {
        upperSpecificationLimit.click();

        upperSpecificationLimit.sendKeys("a");
        return this;
    }

    public CreateCharacteristicPage selectProcess(String processName) {
        new Select(projectSlc).selectByVisibleText(processName);
        return this;
    }

    //Stwórz metodę publiczną o nazwie submitCreate , która klika w przycisk "Create" i zwraca obiekt klasy
    //CharacteristicsPage .

    public CharacteristicsPage submitCreate() {
        cretateCharacteristicsBtn.click();
        return new CharacteristicsPage(driver);
    }

}
