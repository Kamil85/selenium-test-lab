package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardPage {

    WebDriver driver;

    @FindBy(css = ".x_title h2")
    WebElement dashbroardTxt;

    public DashboardPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public DashboardPage assertDashboardHeaderIsShown() {
        Assert.assertTrue(dashbroardTxt.isDisplayed());
        Assert.assertEquals(dashbroardTxt.getText(), "DEMO PROJECT");
        return this;
    }
}
